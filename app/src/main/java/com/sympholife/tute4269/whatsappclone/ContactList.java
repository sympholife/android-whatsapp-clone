package com.sympholife.tute4269.whatsappclone;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class ContactList extends AppCompatActivity {

    // Initialize ListView, ArrayList, and ArrayAdapter
    ListView contactListView;

    ArrayList<String> contacts = new ArrayList<>();
    ArrayAdapter arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        Parse.initialize(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Contact List");

        contactListView = (ListView) findViewById(R.id.contactListView);

        // onCreate, clear the ArrayList
        contacts.clear();

        // initialize adapter and set it to the listView
        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, contacts);
        contactListView.setAdapter(arrayAdapter);

        // Query to get all existing users except the currently logged in. If users exists, add it
        // to the contacts ArrayList and notify changes to the adapter
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereNotEqualTo("username", ParseUser.getCurrentUser().getUsername());
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> objects, ParseException e) {
                if (e == null) {
                    if (objects.size() > 0) {
                        for (ParseUser user: objects) {
                            contacts.add(user.getUsername());
                        }
                        arrayAdapter.notifyDataSetChanged();
                    }
                } else {
                    Log.i("ERROR", e.getMessage());
                }
            }
        });

        // OnClick listener to redirect to the chat, passing the username of the tapped user
        contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), Chat.class);
                i.putExtra("username", contacts.get(position));
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_login_or_signup, menu);

        return super.onCreateOptionsMenu(menu);
    }

    //  Function for when an option menu is selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // When tapping logout, pop up AlertDialog to confirm logging out. Parse.logOut and redirect
        if (item.getItemId() == R.id.action_logout) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Log Out")
                    .setMessage("Are you sure you want to log out?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.i("Option selected", "YES");
                            ParseUser.logOut();
                            Intent intent = new Intent(getApplicationContext(),LoginOrSignup.class);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.i("Option selected", "NO");
                        }
                    }).show();
        }
        return super.onOptionsItemSelected(item);
    }

}
