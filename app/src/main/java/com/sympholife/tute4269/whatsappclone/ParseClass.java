package com.sympholife.tute4269.whatsappclone;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;

public class ParseClass extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        // Add your initialization code here
        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                .applicationId("6y1susmgVDKKkp6kxTMkuT8rLPp72ZJvtHqIUpD8")
                .clientKey("7FSEA7SavLrUa1N89W3ZIl4wJqMHpUd8Gnr0Edx9")
                .server("https://parseapi.back4app.com/")
                .build()
        );


        // ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

    }

}
