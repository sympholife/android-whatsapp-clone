package com.sympholife.tute4269.whatsappclone;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class LoginOrSignup extends AppCompatActivity {

    EditText usernameText, passwordText;
    Button loginButton, signupButton;

    // function to redirect to Contact List if there is a currentUser
    public void redirectIfLoggedIn() {
        if (ParseUser.getCurrentUser() != null) {
            Intent i = new Intent(getApplicationContext(), ContactList.class);
            startActivity(i);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_or_signup);
        Parse.initialize(this);

        usernameText = (EditText) findViewById(R.id.usernameText);
        passwordText = (EditText) findViewById(R.id.passwordText);
        loginButton = (Button) findViewById(R.id.logInButton);
        signupButton = (Button) findViewById(R.id.signUpButton);

        // onCreate, redirect if the user is in Session
        redirectIfLoggedIn();

        ParseAnalytics.trackAppOpenedInBackground(getIntent());
    }

    // When login button is pressed
    public void login(View view) {
        final String username = usernameText.getText().toString();
        String password = passwordText.getText().toString();

        // if there is a field not entered, toast the error. Else, login in background and redirect.
        if (username.equals("") || username.isEmpty() || password.equals("") ||password.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Username or password is missing. Please enter your information.",
                    Toast.LENGTH_SHORT).show();;
        } else {
            ParseUser.logInInBackground(username, password, new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException e) {

                    if (e == null) {
                        Toast.makeText(getApplicationContext(),
                                "Login successful. Welcome, " + username,
                                Toast.LENGTH_SHORT).show();

                        redirectIfLoggedIn();
                    } else {
                        Log.i("ERROR", e.getMessage());
                    }
                }
            });
        }
    }

    // When signup button is pressed
    public void signup(View view) {

        final String username = usernameText.getText().toString();
        String password = passwordText.getText().toString();

        // if there is a field missing, toast the error. Else, signup the user and redirect it.
        if (username.equals("") || username.isEmpty() || password.equals("") || password.isEmpty()) {
            Toast.makeText(getApplicationContext(),
                    "Username or password is missing. Please enter your information.",
                    Toast.LENGTH_SHORT).show();
        } else {
            ParseUser parseUser = new ParseUser();
            parseUser.setUsername(username);
            parseUser.setPassword(password);
            parseUser.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Toast.makeText(getApplicationContext(),
                                "Signed up successfully. Welcome, " + username,
                                Toast.LENGTH_SHORT).show();
                        redirectIfLoggedIn();
                    } else {
                        Log.i("ERROR", e.getMessage());
                    }
                }
            });
        }
    }
}
