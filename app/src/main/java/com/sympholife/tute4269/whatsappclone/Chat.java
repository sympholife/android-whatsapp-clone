package com.sympholife.tute4269.whatsappclone;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

public class Chat extends AppCompatActivity {

    // Initialize EditText, FAB, ListView, ArrayList, and ArrayAdapter
    EditText messageText;
    FloatingActionButton sendChatFAB;

    ListView chatListView;
    ArrayList<String> messages = new ArrayList<>();
    ArrayAdapter arrayAdapter;

    String activeUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Parse.initialize(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // get the username coming from the intent (from the user clicked in Contacts) and assign it
        // to activeUser String, and set the AppBar Title to it.
        Intent intent = getIntent();
        activeUser = intent.getStringExtra("username");
        setTitle(activeUser);

        messageText = (EditText) findViewById(R.id.messageEditText);
        sendChatFAB = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        chatListView = (ListView) findViewById(R.id.chatListView);
        // In here, the arrayAdapter is set to disable tapping it.
        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, messages) {
            public boolean isEnabled(int position)
            {
                return false;
            }
        };

        chatListView.setAdapter(arrayAdapter);

        // First query, getting from "Message" the messages that currentUser has sent to activeUser
        ParseQuery<ParseObject> query1 = new ParseQuery<>("Message");
        query1.whereEqualTo("sender", ParseUser.getCurrentUser().getUsername());
        query1.whereEqualTo("recipient", activeUser);

        // Second query, getting from "Message" the messages that activeUser has sent to currentUser
        ParseQuery<ParseObject> query2 = new ParseQuery<>("Message");
        query2.whereEqualTo("sender", activeUser);
        query2.whereEqualTo("recipient", ParseUser.getCurrentUser().getUsername());

        // List containing both queries
        List<ParseQuery<ParseObject>> queries = new ArrayList<>();
        queries.add(query1);
        queries.add(query2);

        // Query with a ParseQuery.or(). This gets the queries from the list of queries and execute them
        // at the same time. Order it by 'createdAt'. If there are results, clear the messages list,
        // add the 'message' to the list, but if the sender is activeUser, append a "> ". Finally, notify
        // data change to the adapter
        ParseQuery<ParseObject> query = ParseQuery.or(queries);
        query.orderByAscending("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    if (objects.size() > 0) {

                        messages.clear();

                        for (ParseObject message: objects) {
                            String messageContent = message.getString("message");
                            if (!message.getString("sender").equals(ParseUser.getCurrentUser().getUsername())) {
                                messageContent = "> " + messageContent;
                            }
                            messages.add(messageContent);
                            Log.i("MESSAGE", messageContent);
                        }

                        arrayAdapter.notifyDataSetChanged();

                    }
                } else {
                    Log.i("ERROR", e.getMessage());
                }
            }
        });
    }

    public void sendChat(View view) {

        // When sendChat, create ParseObject, with sender as currentUser, recipient as activeUser, and 'message'
        // from the messageText (EditText). When it saves, notify the adapter to update the listView, and
        // delete the MessageText.
        ParseObject message = new ParseObject("Message");
        message.put("sender", ParseUser.getCurrentUser().getUsername());
        message.put("recipient", activeUser);
        message.put("message", messageText.getText().toString());

        final String messageContent = messageText.getText().toString();

        if (!messageContent.equals("")) {
            message.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Toast.makeText(getApplicationContext(), "Message sent!", Toast.LENGTH_SHORT).show();
                        messages.add(messageContent);
                        arrayAdapter.notifyDataSetChanged();
                        messageText.setText("");

                    } else {
                        Log.i("ERROR", e.getMessage());
                    }
                }
            });
        } else {
            messageText.setHint("Enter a message before sending...");
        }
    }

}
